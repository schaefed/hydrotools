#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport sqrt, pi, tan
from libcpp.vector cimport vector
from libcpp.pair cimport pair
from libcpp cimport bool
from libcpp.functional cimport function


cdef extern from "<queue>" namespace "std" nogil:
    cdef cppclass priority_queue[T, Container=*, Compare=*]:
        priority_queue() except +
        priority_queue(Compare&) except +
        bint empty()
        void pop()
        void push(T&)
        size_t size()
        T& top()


cdef extern from "<functional>" namespace "std" nogil:
    cdef cppclass greater[T]:
        bool operator()(T&, T&)


ctypedef pair[double, pair[int, int]] pelement
ctypedef priority_queue[pelement, vector[pelement], greater[pelement]] pqueue
ctypedef fused fdtype:
    double
    float


cdef bool outOfBounds(int y, int x, int nrows, int ncols):
    return (y < 0) or (y >= nrows) or (x < 0) or (x >= ncols)


@cython.boundscheck(False)
@cython.wraparound(False)
# def fillSinks(fdtype[:, :] dem, double fill_value, minslope=0.1):
def fillSinks(np.ndarray[fdtype, ndim=2] dem, double fill_value, minslope=0.1):

    cdef fdtype diag = sqrt(2)
    cdef fdtype deg_to_rad = tan(minslope * pi/180.)
    cdef fdtype[:] epsilon = (np.array([diag, 1, diag, 1, 1, diag, 1, diag],
                                       dtype=dem.dtype)
                              * deg_to_rad)

    cdef int[:] yoffset =   np.array([-1, -1, -1,  0, 0,  1, 1, 1], dtype=np.int32)
    cdef int[:] xoffset =   np.array([-1,  0,  1, -1, 1, -1, 0, 1], dtype=np.int32)

    cdef fdtype[:, :] filled = np.full_like(dem, fill_value)

    cdef pqueue queue
    cdef pelement node

    cdef int nrows = dem.shape[0]
    cdef int ncols = dem.shape[1]
    cdef int nngbrs = yoffset.size

    cdef fdtype spill, ispill
    cdef int y, x, i, iy, ix

    for y in range(nrows):
         for x in range(ncols):
             if dem[y, x] == fill_value:
                 continue
             for i in range(nngbrs):
                 iy = y + yoffset[i]
                 ix = x + xoffset[i]
                 if (outOfBounds(iy, ix, nrows, ncols)
                     or (dem[iy, ix] == fill_value)):
                     spill = dem[y, x]
                     filled[y, x] = spill
                     queue.push((spill, (y, x)))
                     break

    while not queue.empty():
        node = queue.top()
        spill = node.first
        queue.pop()

        for i in range(nngbrs):
            iy = node.second.first + yoffset[i]
            ix = node.second.second + xoffset[i]
            if outOfBounds(iy, ix, nrows, ncols):
                continue
            if (dem[iy, ix] != fill_value) and (filled[iy, ix] == fill_value):
                ispill = dem[iy, ix] + epsilon[i]
                if (ispill < spill):
                    ispill = spill
                queue.push((ispill, (iy, ix)))
                filled[iy, ix] = ispill

    return filled
