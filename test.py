#! /usr/bin/env python
# -*- coding: utf-8 -*-

import random
import numpy as np
import geoarray as ga
import subprocess
from pathlib2 import Path
import requests
import zipfile
import StringIO
from urllib2 import urlopen
from hydrotools import fillSinks
import sys


DEM = "srtm_38_02.tif"

def buildUrls():
    urls = []
    rows = range(1, 25)
    cols = range(1, 73)
    baseurl = "http://srtm.csi.cgiar.org/SRT-ZIP/SRTM_V41/SRTM_Data_GeoTiff/srtm_{row}_{col}.zip"
    
    for row in rows:
        for col in cols:
            urls.append(baseurl.format(row=row, col=col))
            
    return urls

    
def downloadTiles():
    urls = buildUrls()
    random.shuffle(urls)

    for url in urls:
        res = requests.get(url, stream=True)
        if not res.ok:
            continue
        zfile = zipfile.ZipFile(StringIO.StringIO(res.content))
        members = [f for f in zfile.namelist() if f.endswith("tif")]
        zfile.extractall(members=members)
        print "downloaded: ", members
        break


    
def sagaFill(fname):
    subprocess.call(
        "saga_cmd ta_preprocessor 4 -MINSLOPE 0.1 -ELEV {:} -FILLED filled.sgrd".format(fname),
        shell=True)
    return ga.fromfile("filled.sdat")


if __name__ == "__main__":

    # downloadTiles()

    if len(sys.argv) != 2:
        raise RuntimeError("Exactly one argument needed (the DEM file)!")

    dem = sys.argv[1]

    other = sagaFill(dem)

    dem = ga.fromfile(dem).astype(np.float32)

    filled = fillSinks(dem.filled(other.fill_value), other.fill_value, minslope=0.1)
    this = ga.array(filled, fill_value=other.fill_value)

    # no slope imposed on fillSinks yet
    import pdb; pdb.set_trace()

    assert (this == other).all()
