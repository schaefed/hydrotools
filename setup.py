#! /usr/bin/env python
# -*- coding: utf-8 -*-
from distutils.core import setup, Extension
from Cython.Build import cythonize
import numpy as np

setup(
    ext_modules = cythonize(
        Extension(
            name = "hydrotools",
            sources = ["hydrotools.pyx"],
            language="c++",
        )),
    include_dirs = [np.get_include()]
)
